from tkinter import *
from evasdk import Eva
import os

eva = Eva(os.getenv('EVA_HOST'), os.getenv('EVA_TOKEN'))
toolpaths = eva.toolpaths_list()


# Import module

# Create object
root = Tk()

# Adjust size
root.geometry("500x500")


def get_selected_toolpath():
    toolpath_name = toolpath_clicked.get()
    for toolpath in toolpaths:
        if toolpath['name'] == toolpath_name:
            toolpath_id = toolpath['id']
            selected_toolpath = eva.toolpaths_retrieve(toolpath_id)
            return selected_toolpath


def get_selected_point_radians():
    selected_point = points_clicked.get()
    selected_toolpath = get_selected_toolpath()
    waypoints = selected_toolpath["toolpath"]["waypoints"]
    for waypoint in waypoints:
        if waypoint["label_id"] == (int(selected_point)+1):
            radians = waypoint["joints"]
            return radians


def save_path():
    selected_point = points_clicked.get()
    selected_toolpath = get_selected_toolpath()
    print('selected_toolpath: ', selected_toolpath)
    waypoints = selected_toolpath["toolpath"]["waypoints"]
    for waypoint in waypoints:
        if waypoint["label_id"] == (int(selected_point)+1):
            waypoint['joints'] = calculate_new_point()
    eva.toolpaths_save(
        name=selected_toolpath['name'], toolpath=selected_toolpath['toolpath'])
    # eva.toolpaths_save
    # pass


def calculate_new_point():
    guess = eva.data_servo_positions()

    new_coords = {'x': float(x_pos_var.get()),
                  'y': float(y_pos_var.get()),
                  'z': float(z_pos_var.get())}

    new_orientation = {'w': float(w_orien_var.get()),
                       'x': float(x_orien_var.get()),
                       'y': float(y_orien_var.get()),
                       'z': float(z_orien_var.get())}

    print('new_coords: ', new_coords)
    print('new_orientation: ', new_orientation)

    new_point = eva.calc_inverse_kinematics(
        guess, new_coords, new_orientation)
    return new_point


def go_to_new_point():

    new_point = calculate_new_point()

    with eva.lock():
        eva_state = eva.data_snapshot()['control']['state']
        if eva_state == 'error':
            eva.control_reset_errors()
        if eva_state == 'collision':
            eva.control_acknowledge_collision()

        eva.control_wait_for_ready()
        eva.control_go_to(joints=new_point, max_speed=0.1, mode='automatic')
        curr_rad = eva.data_servo_positions()
        curr_coords = eva.calc_forward_kinematics(curr_rad)
        x_pos_var.set(curr_coords['position']['x'])
        y_pos_var.set(curr_coords['position']['y'])
        z_pos_var.set(curr_coords['position']['z'])
        w_orien_var.set(curr_coords['orientation']['w'])
        x_orien_var.set(curr_coords['orientation']['x'])
        y_orien_var.set(curr_coords['orientation']['y'])
        z_orien_var.set(curr_coords['orientation']['z'])
        print('curr_coords: ', curr_coords)


x_pos_var = StringVar()
y_pos_var = StringVar()
z_pos_var = StringVar()

w_orien_var = StringVar()
x_orien_var = StringVar()
y_orien_var = StringVar()
z_orien_var = StringVar()


def go_to_point():
    selected_point = points_clicked.get()
    selected_toolpath = get_selected_toolpath()
    waypoints = selected_toolpath["toolpath"]["waypoints"]
    for waypoint in waypoints:
        if waypoint["label_id"] == (int(selected_point)+1):
            print('waypoint: ', waypoint)
            radians = waypoint["joints"]
            coords = eva.calc_forward_kinematics(radians)
            # coords = eva.data_servo_positions()
            position = coords['position']
            pos_x = position['x']
            pos_y = position['y']
            pos_z = position['z']

            x_pos_label = Label(root, text="Pos X: ")
            x_pos_label.grid(column=0, row=6)
            x_pos_entry = Entry(root, textvariable=x_pos_var,
                                font=('calibre', 10, 'normal'))
            x_pos_var.set(pos_x)
            x_pos_entry.grid(column=1, row=6)

            y_pos_label = Label(root, text="Pos Y: ")
            y_pos_label.grid(column=0, row=7)
            y_pos_entry = Entry(root, textvariable=y_pos_var,
                                font=('calibre', 10, 'normal'))
            y_pos_var.set(pos_y)
            y_pos_entry.grid(column=1, row=7)

            z_pos_label = Label(root, text="Pos Z: ")
            z_pos_label.grid(column=0, row=8)
            z_pos_entry = Entry(root, textvariable=z_pos_var,
                                font=('calibre', 10, 'normal'))
            z_pos_var.set(pos_z)
            z_pos_entry.grid(column=1, row=8)

            orientation = coords['orientation']
            orien_w = orientation['w']
            orien_x = orientation['x']
            orien_y = orientation['y']
            orien_z = orientation['z']

            w_orien_label = Label(root, text="Orientation W: ")
            w_orien_label.grid(column=0, row=9)
            w_orien_entry = Entry(root, textvariable=w_orien_var,
                                  font=('calibre', 10, 'normal'))
            w_orien_var.set(orien_w)
            w_orien_entry.grid(column=1, row=9)

            x_orien_label = Label(root, text="Orientation X: ")
            x_orien_label.grid(column=0, row=10)
            x_orien_entry = Entry(root, textvariable=x_orien_var,
                                  font=('calibre', 10, 'normal'))
            x_orien_var.set(orien_x)
            x_orien_entry.grid(column=1, row=10)

            y_orien_label = Label(root, text="Orientation Y: ")
            y_orien_label.grid(column=0, row=11)
            y_orien_entry = Entry(root, textvariable=y_orien_var,
                                  font=('calibre', 10, 'normal'))
            y_orien_var.set(orien_y)
            y_orien_entry.grid(column=1, row=11)

            z_orien_label = Label(root, text="Orientation Z: ")
            z_orien_label.grid(column=0, row=12)
            z_orien_entry = Entry(root, textvariable=z_orien_var,
                                  font=('calibre', 10, 'normal'))
            z_orien_var.set(orien_z)
            z_orien_entry.grid(column=1, row=12)

            btnGoToNew = Button(root, text="Go to new",
                                command=go_to_new_point)
            btnGoToNew.grid(column=0, row=13)

            btnSave = Button(root, text="Save path",
                             command=save_path)
            btnSave.grid(column=1, row=13)

            print('current pos: ', coords)
            print('current z coord: ', coords['orientation']['z'])
            with eva.lock():
                eva_state = eva.data_snapshot()['control']['state']
                if eva_state == 'error':
                    eva.control_reset_errors()
                if eva_state == 'collision':
                    eva.control_acknowledge_collision()

                eva.control_wait_for_ready()
                eva.control_home()
                eva.control_go_to(joints=radians)
                break


def load_point():
    points_label = Label(root, text=points_clicked.get())
    points_label.grid(column=1, row=4)

    btnGoTo = Button(root, text="Go to", command=go_to_point)
    btnGoTo.grid(column=1, row=5)


# datatype of menu text
points_clicked = StringVar()


def load():
    selected_toolpath = get_selected_toolpath()
    timeline = selected_toolpath["toolpath"]["timeline"]
    points_options = []
    for item in timeline:
        if item['type'] != "output-set":
            print('item: ', item)
            points_options.append(item["waypoint_id"])

    # initial menu text
    points_clicked.set(points_options[0])

    btnRun = Button(root, text="Run", command=run_path)
    btnRun.grid(column=1, row=2)

    points_label = Label(root, text="Select point: ")
    points_label.grid(column=0, row=3)

    dropPoints = OptionMenu(root, points_clicked, *points_options)
    dropPoints.grid(column=1, row=3)

    btnLoadPoint = Button(root, text="Load point", command=load_point)
    btnLoadPoint.grid(column=2, row=3)

    toolpath_label.config(text=toolpath_clicked.get())


def run_path():
    selected_toolpath = get_selected_toolpath()

    with eva.lock():

        eva_state = eva.data_snapshot()['control']['state']
        if eva_state == 'error':
            eva.control_reset_errors()
        if eva_state == 'collision':
            eva.control_acknowledge_collision()

        eva.control_wait_for_ready()

        print('selected_toolpath: ', selected_toolpath)
        eva.toolpaths_use(selected_toolpath['toolpath'])
        eva.control_home()
        eva.control_run(loop=1)


toolpath_label = Label(root, text="Select toolpath: ")
toolpath_label.grid(column=0, row=0)

# Dropdown menu options
toolpath_options = []

for toolpath in toolpaths:
    toolpath_options.append(toolpath['name'])

# datatype of menu text
toolpath_clicked = StringVar()

# initial menu text
toolpath_clicked.set(toolpath_options[0])


# Create Dropdown menu
dropToolpath = OptionMenu(root, toolpath_clicked, *toolpath_options)
dropToolpath.grid(column=1, row=0)

# Create button, it will change label text
button = Button(root, text="Load path", command=load)
button.grid(column=2, row=0)

# Create Label
toolpath_label = Label(root, text=" ")
toolpath_label.grid(column=1, row=1)


# Execute tkinter
root.mainloop()
